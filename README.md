## Ansible 

Date : 2016-03-11

This is slides of an introduction talk about Ansible (in french).
Slides are based on Shower Presentation Engine.

### Shower Presentation Template

<img src="pictures/logo.png" width="250" alt="Shower logo">

> Shower ['ʃəuə] noun. A person or thing that shows.

1. Built on HTML, CSS and vanilla JavaScript
2. Works in all modern browsers
3. Themes are separated from engine
4. Modular and extensible
5. Fully keyboard accessible
6. Printable to PDF

[See it in action](http://shwr.me/). Includes [Ribbon](https://github.com/shower/ribbon/) and [Material](https://github.com/shower/material/) themes, and [core](https://github.com/shower/core/) with plugins.

Follow [@shower_me](https://twitter.com/shower_me) for support and updates, [file an issue](https://github.com/shower/shower/issues/new) if you have any.


